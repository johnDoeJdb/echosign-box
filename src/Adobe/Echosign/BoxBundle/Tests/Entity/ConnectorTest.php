<?php

namespace Adobe\Echosign\BoxBundle\Tests\Entity;

use Adobe\Echosign\BoxBundle\Api\EchoSign;
use Adobe\Echosign\BoxBundle\Model\Registrator;
use Adobe\Echosign\BoxBundle\Model\Authenticator;

class ConnectorTest extends \PHPUnit_Framework_TestCase
{
    public function testRegisterUser()
    {
        $API = new EchoSign();
        $user = new Registrator();

        $user
            ->setEmail("echosign@go2.pl")
            ->setPassword("johndoe")
            ->setFirstName("John")
            ->setLastName("Doe");

        // Cannot test successful, so let's test almost successful
        $this->assertFalse($API->registerUser($user));
        $this->assertEquals("EXISTING_USER", $API->getLastStatus());
    }

    public function testIssueAccessToken()
    {
        $API = new EchoSign();
        $echosignUser = new Authenticator();

        // Successful authentication
        $echosignUser
            ->setEmail("echosign@go2.pl")
            ->setPassword("johndoe");

        $token = $API->issueAccessToken($echosignUser);
        $this->assertRegExp("/^[a-zA-Z0-9_\-]{32,}/", $token);

        // Unsuccessful authentication
        $echosignUser
            ->setPassword("wrong_password");
        $token = $API->issueAccessToken($echosignUser);
        $this->assertFalse($token);
        $this->assertEquals("INVALID_USER", $API->getLastStatus());
    }
}