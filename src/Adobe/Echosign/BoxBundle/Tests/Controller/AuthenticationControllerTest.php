<?php

namespace Adobe\Echosign\BoxBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AuthenticationControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request("GET", "/box/login/");

        $this->assertTrue(
            $client->getResponse()->isSuccessful(),
            "Login page did not load properly"
        );

        // Test form validation
        $form = $crawler->selectButton("Sign In")->form();
        $form["authentication[email]"] = "wrong_email";
        $form["authentication[password]"] = "wrps";

        $crawler = $client->submit($form);

        $this->assertEquals(
            "You entered an invalid email address",
            $crawler->filter("form fieldset ol li ul li")->eq(0)->text()
        );
        $this->assertEquals(
            "Please use a password that is at least six characters long",
            $crawler->filter("form fieldset ol li ul li")->eq(1)->text()
        );

        // Test EchoSign validation (wrong credentials)
        $form = $crawler->selectButton("Sign In")->form();
        $form["authentication[email]"] = "echosign@go2.pl";
        $form["authentication[password]"] = "wrong_password";

        $crawler = $client->submit($form);

        $this->assertEquals(
            "Email and password do not match.",
            $crawler->filter("#flash-error")->text()
        );

        // Test EchoSign validation (valid credentials)
        $form = $crawler->selectButton("Sign In")->form();
        $form["authentication[email]"] = "echosign@go2.pl";
        $form["authentication[password]"] = "johndoe";

        $crawler = $client->submit($form);

        $this->assertTrue(
            $client->getResponse()->isRedirect(),
            "Successful login should have redirected."
        );

        $crawler = $client->followRedirect();

        $this->assertEquals(
            "You have been successfully logged in.",
            $crawler->filter("#flash-success")->text()
        );
    }
}
