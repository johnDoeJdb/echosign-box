<?php

namespace Adobe\Echosign\BoxBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RegistrationControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/box/registration/');

        $this->assertTrue(
            $client->getResponse()->isSuccessful(),
            "Registration page did not load properly"
        );

        // Test form validation
        $form = $crawler->selectButton("Create my account")->form();
        $form["registration[firstName]"]  = "1-wrong-character";
        $form["registration[lastName]"]   = "1-wrong-character";
        $form["registration[email]"]      = "wrong-email";
        $form["registration[password]"]   = "12345"; // too short
        $form["registration[passwordConfirm]"]  = "54321"; // too short and different
        //$form["registration[termsAgree]"] // field not checked

        $crawler = $client->submit($form);

        $errors = $crawler->filter("form fieldset ul li");

        $this->assertGreaterThan(
            0,
            $errors->count(),
            "Some errors should have been raised"
        );

        $this->assertEquals(
            "Your first name contains illegal characters",
            $errors->eq(0)->text()
        );
        $this->assertEquals(
            "Your last name contains illegal characters",
            $errors->eq(1)->text()
        );
        $this->assertEquals(
            "You entered an invalid email address",
            $errors->eq(2)->text()
        );
        $this->assertEquals(
            "Please use a password that is at least six characters long",
            $errors->eq(3)->text()
        );
        $this->assertEquals(
            "Please use a password that is at least six characters long",
            $errors->eq(4)->text()
        );
        $this->assertEquals(
            "Your passwords do not match; please re-enter both passwords",
            $errors->eq(5)->text()
        );
        $this->assertEquals(
            "You must read and agree to the EchoSign Terms of Use and Privacy Policy before creating an account",
            $errors->eq(6)->text()
        );

        // Test a valid form
        $form = $crawler->selectButton("Create my account")->form();
        $form["registration[firstName]"]  = "John";
        $form["registration[lastName]"]   = "Doe";
        $form["registration[email]"]      = "echosign@go2.pl";
        $form["registration[password]"]   = "johndoe";
        $form["registration[passwordConfirm]"] = "johndoe";
        $form["registration[termsAgree]"] = "1";

        $crawler = $client->submit($form);

        $this->assertEquals(
            0,
            $crawler->filter("form fieldset ul li")->count(),
            "No errors should have been raised"
        );

        $this->assertEquals(
            "A user with the specified email already exists.",
            $crawler->filter("#flash-error")->text()
        );

    }

}
