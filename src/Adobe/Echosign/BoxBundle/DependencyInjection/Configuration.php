<?php

namespace Adobe\Echosign\BoxBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $treeBuilder
            ->root('adobe_echosign_box')
            ->children()
                ->arrayNode('box')
                ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('client_id')->defaultValue('')->end()
                        ->scalarNode('secret')->defaultValue('')->end()
                        ->scalarNode('redirect_uri')->defaultValue('')->end()
                        ->scalarNode('auth_host')->defaultValue('')->end()
                        ->scalarNode('api_host')->defaultValue('')->end()
                        ->scalarNode('file_upload_host')->defaultValue('')->end()
                    ->end()
                ->end()
                ->arrayNode('echo_sign')
                ->addDefaultsIfNotSet()
                ->children()
                ->arrayNode('doc')
                ->addDefaultsIfNotSet()
                ->children()
                ->scalarNode('wsdl')->defaultValue('')->end()
                ->end()
                ->end()
                ->arrayNode('auth')
                    ->addDefaultsIfNotSet()
                    ->children()
                    ->scalarNode('client_id')->defaultValue('')->end()
                    ->scalarNode('client_secret')->defaultValue('')->end()
                    ->scalarNode('redirect_url')->defaultValue('')->end()
                    ->scalarNode('host')->defaultValue('')->end()
                ->end()
                ->end()
                ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
