<?php
namespace Adobe\Echosign\BoxBundle\DependencyInjection\Session;

use Symfony\Component\HttpFoundation\Session\Storage\Proxy\SessionHandlerProxy;

class EncryptedSessionProxy extends SessionHandlerProxy
{
    protected $key;

    public function __construct(\SessionHandlerInterface $handler)
    {
        if (isset($_SERVER["HTTP_USER_AGENT"])) // yeah, I know...
        {
            $salt = $_SERVER["HTTP_USER_AGENT"]; 
        }
        else
        {
            $salt = "Symfony2";
        }
        
        $this->key = substr(md5($this->getId() . $salt), 0, 8);

        parent::__construct($handler);
    }

    public function read($id)
    {
        $data = parent::read($id);

        return mcrypt_decrypt(\MCRYPT_3DES, $this->key, $data, \MCRYPT_MODE_ECB);
    }

    public function write($id, $data)
    {
        $data = mcrypt_encrypt(\MCRYPT_3DES, $this->key, $data, \MCRYPT_MODE_ECB);

        return parent::write($id, $data);
    }
}
