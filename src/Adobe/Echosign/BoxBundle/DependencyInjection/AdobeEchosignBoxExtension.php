<?php

namespace Adobe\Echosign\BoxBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class AdobeEchosignBoxExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $container->setParameter('box.client_id', $config['box']['client_id']);
        $container->setParameter('box.secret', $config['box']['secret']);
        $container->setParameter('box.redirect_uri', $config['box']['redirect_uri']);
        $container->setParameter('box.api_host', $config['box']['api_host']);
        $container->setParameter('box.auth_host', $config['box']['auth_host']);
        $container->setParameter('box.file_upload_host', $config['box']['file_upload_host']);

        $container->setParameter('echo_sign.doc.wsdl', $config['echo_sign']['doc']['wsdl']);
        $container->setParameter('echo_sign.auth.client_id', $config['echo_sign']['auth']['client_id']);
        $container->setParameter('echo_sign.auth.client_secret', $config['echo_sign']['auth']['client_secret']);
        $container->setParameter('echo_sign.auth.redirect_url', $config['echo_sign']['auth']['redirect_url']);
        $container->setParameter('echo_sign.auth.host', $config['echo_sign']['auth']['host']);
    }
}
