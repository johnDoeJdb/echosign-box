<?php

namespace Adobe\Echosign\BoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BoxUserStorage
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Adobe\Echosign\BoxBundle\Entity\BoxUserRepository")
 */
class BoxUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="userId", type="string", length=255)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255)
     */
    private $token;

    /**
     * @var string
     *
     * @ORM\Column(name="refreshToken", type="string", length=255)
     */
    private $refreshToken;

    /**
     * @var string
     *
     * @ORM\Column(name="tokenInitialized", type="datetime")
     */
    private $tokenInitialized;

    /**
     * @var string
     *
     * @ORM\Column(name="refreshTokenInitialized", type="datetime")
     */
    private $refreshTokenInitialized;

    /**
     * @var string
     *
     * @ORM\Column(name="synced", type="boolean", nullable=true)
     */
    private $synced;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="EchoSignUser", mappedBy="BoxUser")
     */
    private $echoSignUser;

    /**
     * @var string
     *
     * @ORM\Column(name="last_scan", type="datetime", nullable=true)
     */
    private $lastScan;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->echoSignUser = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param string $userId
     * @return BoxUser
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    
        return $this;
    }

    /**
     * Get userId
     *
     * @return string 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return BoxUser
     */
    public function setToken($token)
    {
        $this->token = $token;
    
        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set refreshToken
     *
     * @param string $refreshToken
     * @return BoxUser
     */
    public function setRefreshToken($refreshToken)
    {
        $this->refreshToken = $refreshToken;
    
        return $this;
    }

    /**
     * Get refreshToken
     *
     * @return string 
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * Set tokenInitialized
     *
     * @param \DateTime $tokenInitialized
     * @return BoxUser
     */
    public function setTokenInitialized($tokenInitialized)
    {
        $this->tokenInitialized = $tokenInitialized;
    
        return $this;
    }

    /**
     * Get tokenInitialized
     *
     * @return \DateTime 
     */
    public function getTokenInitialized()
    {
        return $this->tokenInitialized;
    }

    /**
     * Set refreshTokenInitialized
     *
     * @param \DateTime $refreshTokenInitialized
     * @return BoxUser
     */
    public function setRefreshTokenInitialized($refreshTokenInitialized)
    {
        $this->refreshTokenInitialized = $refreshTokenInitialized;
    
        return $this;
    }

    /**
     * Get refreshTokenInitialized
     *
     * @return \DateTime 
     */
    public function getRefreshTokenInitialized()
    {
        return $this->refreshTokenInitialized;
    }

    /**
     * Set synced
     *
     * @param boolean $synced
     * @return BoxUser
     */
    public function setSynced($synced)
    {
        $this->synced = $synced;
    
        return $this;
    }

    /**
     * Get synced
     *
     * @return boolean 
     */
    public function getSynced()
    {
        return $this->synced;
    }

    /**
     * Add echoSignUser
     *
     * @param \Adobe\Echosign\BoxBundle\Entity\EchoSignUser $echoSignUser
     * @return BoxUser
     */
    public function addEchoSignUser(\Adobe\Echosign\BoxBundle\Entity\EchoSignUser $echoSignUser)
    {
        $this->echoSignUser[] = $echoSignUser;
    
        return $this;
    }

    /**
     * Remove echoSignUser
     *
     * @param \Adobe\Echosign\BoxBundle\Entity\EchoSignUser $echoSignUser
     */
    public function removeEchoSignUser(\Adobe\Echosign\BoxBundle\Entity\EchoSignUser $echoSignUser)
    {
        $this->echoSignUser->removeElement($echoSignUser);
    }

    /**
     * Get echoSignUser
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEchoSignUser()
    {
        return $this->echoSignUser;
    }

    /**
     * @return \DateTime
     */
    public function getLastScan()
    {
        return $this->lastScan;
    }

    /**
     * @param \DateTime
     * @return BoxUser
     */
    public function setLastScan($lastScan)
    {
        $this->lastScan = $lastScan;
    }
}