<?php

namespace Adobe\Echosign\BoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Documents
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Adobe\Echosign\BoxBundle\Entity\DocumentRepository")
 */
class Document
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="waitingSince", type="text", length=255, nullable=true)
     */
    private $waitingSince;

    /**
     * @var string
     *
     * @ORM\Column(name="documentKey", type="string", length=255)
     */
    private $documentKey;

    /**
     * @var string
     *
     * @ORM\Column(name="box_file_id", type="string", length=255, nullable=true)
     */
    private $boxFileId;

    /**
     * @var string
     *
     * @ORM\Column(name="lastStatus", type="string", length=255, nullable=true)
     */
    private $lastStatus;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="EchoSignUser", inversedBy="documents", cascade={"all"})
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Folder", inversedBy="document", cascade={"all"})
     */
    private $folder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="queued", type="boolean", nullable=true)
     */
    private $queued;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return boolean
     */
    public function isQueued()
    {
        return $this->queued;
    }

    /**
     * @param boolean $queued
     */
    public function setQueued($queued)
    {
        $this->queued = $queued;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Document
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set waitingSince
     *
     * @param string $waitingSince
     * @return Document
     */
    public function setWaitingSince($waitingSince)
    {
        $this->waitingSince = $waitingSince;
    
        return $this;
    }

    /**
     * Get waitingSince
     *
     * @return string 
     */
    public function getWaitingSince()
    {
        return $this->waitingSince;
    }

    /**
     * Set documentKey
     *
     * @param string $documentKey
     * @return Document
     */
    public function setDocumentKey($documentKey)
    {
        $this->documentKey = $documentKey;
    
        return $this;
    }

    /**
     * Get documentKey
     *
     * @return string 
     */
    public function getDocumentKey()
    {
        return $this->documentKey;
    }

    /**
     * Set boxFileId
     *
     * @param string $boxFileId
     * @return Document
     */
    public function setBoxFileId($boxFileId)
    {
        $this->boxFileId = $boxFileId;
    
        return $this;
    }

    /**
     * Get boxFileId
     *
     * @return string 
     */
    public function getBoxFileId()
    {
        return $this->boxFileId;
    }

    /**
     * Set lastStatus
     *
     * @param string $lastStatus
     * @return Document
     */
    public function setLastStatus($lastStatus)
    {
        $this->lastStatus = $lastStatus;
    
        return $this;
    }

    /**
     * Get lastStatus
     *
     * @return string 
     */
    public function getLastStatus()
    {
        return $this->lastStatus;
    }

    /**
     * Set user
     *
     * @param \Adobe\Echosign\BoxBundle\Entity\EchoSignUser $user
     * @return Document
     */
    public function setUser(\Adobe\Echosign\BoxBundle\Entity\EchoSignUser $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Adobe\Echosign\BoxBundle\Entity\EchoSignUser 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set folder
     *
     * @param \Adobe\Echosign\BoxBundle\Entity\Folder $folder
     * @return Document
     */
    public function setFolder(\Adobe\Echosign\BoxBundle\Entity\Folder $folder = null)
    {
        $this->folder = $folder;
    
        return $this;
    }

    /**
     * Get folder
     *
     * @return \Adobe\Echosign\BoxBundle\Entity\Folder
     */
    public function getFolder()
    {
        return $this->folder;
    }
}