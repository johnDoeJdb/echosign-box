<?php

namespace Adobe\Echosign\BoxBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * DocumentsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class EchoSignUserRepository extends EntityRepository
{
    public function fetchEchoSignUser($boxUserId)
    {
        $result = $this->createQueryBuilder('e')
            ->select()
            ->leftJoin('e.boxUser', 'b')
            ->where('b.userId = :id')
            ->setParameter('id', $boxUserId)
            ->getQuery()
            ->getResult();

        $user = $result ? $result[0] : null;

        return $user;
    }

    public function getEchoSignUserByDocument($id)
    {
        return $this->createQueryBuilder('e')
            ->select()
            ->leftJoin('e.documents', 'd')
            ->where('d.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }
}
