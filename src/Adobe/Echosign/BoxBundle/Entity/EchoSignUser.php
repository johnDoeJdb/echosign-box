<?php

namespace Adobe\Echosign\BoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\VarDumper\Cloner\Data;

/**
 * EchoSignUserStorage
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Adobe\Echosign\BoxBundle\Entity\EchoSignUserRepository")
 */
class EchoSignUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255, nullable=true)
     */
    private $token;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="refresh_token", type="string", length=255, nullable=true)
     */
    private $refreshToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expire_token", type="datetime", nullable=true)
     */
    private $expireToken;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="BoxUser", inversedBy="echoSignUser")
     */
    private $boxUser;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="Document", mappedBy="user", cascade={"all"})
     */
    private $documents;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->documents = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getExpireToken()
    {
        return $this->expireToken;
    }

    /**
     * @param \DateTime $expireToken
     */
    public function setExpireToken(\DateTime $expireToken)
    {
        $this->expireToken = $expireToken;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

     /**
     * Set token
     *
     * @param string $token
     * @return EchoSignUser
     */
    public function setToken($token)
    {
        $this->token = $token;
    
        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * @param string $refreshToken
     */
    public function setRefreshToken($refreshToken)
    {
        $this->refreshToken = $refreshToken;
    }

    /**
     * Set boxUser
     *
     * @param \Adobe\Echosign\BoxBundle\Entity\BoxUser $boxUser
     * @return EchoSignUser
     */
    public function setBoxUser(\Adobe\Echosign\BoxBundle\Entity\BoxUser $boxUser = null)
    {
        $this->boxUser = $boxUser;
    
        return $this;
    }

    /**
     * Get boxUser
     *
     * @return \Adobe\Echosign\BoxBundle\Entity\BoxUser 
     */
    public function getBoxUser()
    {
        return $this->boxUser;
    }

    /**
     * Add documents
     *
     * @param \Adobe\Echosign\BoxBundle\Entity\Document $documents
     * @return EchoSignUser
     */
    public function addDocument(\Adobe\Echosign\BoxBundle\Entity\Document $documents)
    {
        $this->documents[] = $documents;
    
        return $this;
    }

    /**
     * Remove documents
     *
     * @param \Adobe\Echosign\BoxBundle\Entity\Document $documents
     */
    public function removeDocument(\Adobe\Echosign\BoxBundle\Entity\Document $documents)
    {
        $this->documents->removeElement($documents);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    public function isValidToken()
    {
        $now = new \DateTime();
        $tokenExpires = $this->getExpireToken();
        $isValid = $tokenExpires ? ($tokenExpires > $now) : false;

        return $isValid;
    }
}