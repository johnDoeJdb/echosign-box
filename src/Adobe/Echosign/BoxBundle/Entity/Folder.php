<?php

namespace Adobe\Echosign\BoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Documents
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Adobe\Echosign\BoxBundle\Entity\FolderRepository")
 */
class Folder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="boxId", type="bigint", nullable=true)
     */
    private $boxId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="Document", mappedBy="folder", cascade={"all"})
     */
    private $document;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->document = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set boxId
     *
     * @param integer $boxId
     * @return Folder
     */
    public function setBoxId($boxId)
    {
        $this->boxId = $boxId;
    
        return $this;
    }

    /**
     * Get boxId
     *
     * @return integer 
     */
    public function getBoxId()
    {
        return $this->boxId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Folder
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add document
     *
     * @param \Adobe\Echosign\BoxBundle\Entity\Document $document
     * @return Folder
     */
    public function addDocument(\Adobe\Echosign\BoxBundle\Entity\Document $document)
    {
        $this->document[] = $document;
    
        return $this;
    }

    /**
     * Remove document
     *
     * @param \Adobe\Echosign\BoxBundle\Entity\Document $document
     */
    public function removeDocument(\Adobe\Echosign\BoxBundle\Entity\Document $document)
    {
        $this->document->removeElement($document);
    }

    /**
     * Get document
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocument()
    {
        return $this->document;
    }
}