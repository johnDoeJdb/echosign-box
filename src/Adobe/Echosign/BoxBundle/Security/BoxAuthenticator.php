<?php

namespace Adobe\Echosign\BoxBundle\Security;

use Adobe\Echosign\BoxBundle\Api\Box;
use Adobe\Echosign\BoxBundle\Api\EchoSign;
use Adobe\Echosign\BoxBundle\Entity\EchoSignUser;
use Adobe\Echosign\BoxBundle\Manager\CryptManager;
use Adobe\Echosign\BoxBundle\Manager\QueueManager;
use Adobe\Echosign\BoxBundle\Manager\RestoreManager;
use Adobe\Echosign\BoxBundle\Manager\UserManager;
use Adobe\Echosign\BoxBundle\Manager\WindowManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\SimplePreAuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Adobe\Echosign\BoxBundle\Model\BoxRequest;
use Adobe\Echosign\BoxBundle\Validator\BoxRequestValidator;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class BoxAuthenticator implements SimplePreAuthenticatorInterface, AuthenticationFailureHandlerInterface, AuthenticationSuccessHandlerInterface
{
    protected $userProvider;
    protected $session;
    protected $userManager;
    protected $container;
    protected $boxApiWrapper;
    protected $echoSignApiWrapper;
    protected $requestStack;
    protected $cryptManager;
    protected $authenticationType;
    protected $queueManager;
    protected $restoreManager;

    public function __construct(
        BoxUserProvider $userProvider,
        Session $session,
        UserManager $userManager,
        Box $boxApiWrapper,
        EchoSign $echoSignApiWrapper,
        RequestStack $requestStack,
        CryptManager $cryptManager,
        QueueManager $queueManager,
        RestoreManager $restoreManager
    )
    {
        $this->userProvider = $userProvider;
        $this->session = $session;
        $this->userManager= $userManager;
        $this->boxApiWrapper = $boxApiWrapper;
        $this->echoSignApiWrapper = $echoSignApiWrapper;
        $this->requestStack = $requestStack;
        $this->cryptManager = $cryptManager;
        $this->queueManager = $queueManager;
        $this->restoreManager = $restoreManager;
    }

    public function createToken(Request $request, $providerKey)
    {
        $requestParameters = $request->query;
        if ($requestParameters->count()) {
            $box = new BoxRequest($request->query);
        } else {
            $box = unserialize($this->session->get('box'));
        }

        if (!$box instanceof BoxRequest || !BoxRequestValidator::validate($box)) {
            throw new BadCredentialsException('No required parameters found in request');
        }

        $this->session->start();
        $this->session->set("box", serialize($box));

        return new PreAuthenticatedToken(
            'anon.',
            $box,
            $providerKey
        );
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        $box = $token->getCredentials();
        $this->boxAuthentication($box);
        $this->echoSignAuthentication($box);

        $user = $this->userProvider->loadUserByUsername($box->getUserName());

        return new PreAuthenticatedToken(
            $user,
            $box,
            $providerKey,
            $user->getRoles()
        );
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $box = unserialize($this->session->get("box"));
        if (!$box instanceof BoxRequest || !BoxRequestValidator::validate($box)) {
            return new Response("Missed require parameters from request", 403);
        }
        $encryptedState = $this->cryptManager->serializeAndCrypt($box);
        if ($this->authenticationType === "Box") {
            return new RedirectResponse($this->boxApiWrapper->getAuthUrl($encryptedState, $request->server->get('HTTP_REFERER')));
        } else {
            return new RedirectResponse($this->echoSignApiWrapper->getAuthorizationOAuthUrl($encryptedState, $request->server->get('HTTP_REFERER')));
        }
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        // Restore deleted folders
        $boxUser = $this->userManager->fetchCurrentBoxUser();
        $boxToken = $this->userManager->getTokenForBoxUser($boxUser);
        $this->restoreManager->restoreFoldersIfTrashed($boxUser, $boxToken);

        // Download queued documents
        $this->queueManager->downloadQueuedDocuments($boxUser);
    }

    private function boxAuthentication(BoxRequest $box)
    {
        $this->authenticationType = "Box";
        if (!$boxUser = $this->userManager->findBoxUser($box->getUserId())) {
            throw new AuthenticationException(
                'Box user not found in database'
            );
        }

        if (!$this->userManager->getTokenForBoxUser($boxUser)) {
            throw new AuthenticationException(
                'Invalid or expired token'
            );
        }
    }

    private function echoSignAuthentication()
    {
        $this->authenticationType = "EchoSign";

        $echoSignUser = $this->userManager->fetchCurrentEchoSignUser();

        if (!$echoSignUser) {
            throw new AuthenticationException(
                'EchoSign user not found in database'
            );
        }

        if (!$echoSignUser instanceof EchoSignUser) {
            throw new AuthenticationException(
                'Invalid EchoSign user object'
            );
        }

        if (!$echoSignUser->getToken() || !$echoSignUser->isValidToken()) {
            if ($echoSignUser->getRefreshToken()) {
                $refreshToken = $this->userManager->getRefreshTokenForEchoSignUser($echoSignUser);
                $refreshedToken = $this->echoSignApiWrapper->refreshOAuthToken($refreshToken);
                if (property_exists($refreshedToken, 'access_token') && property_exists($refreshedToken, 'expires_in')) {
                    $accessToken = $refreshedToken->access_token;
                    $expirationIn = sprintf('+%d seconds', $refreshedToken->expires_in);
                    $expiration = (new \DateTime())->modify($expirationIn);
                    $this->userManager->updateEchoSignToken($accessToken, $expiration);

                    return;
                }
            }
            throw new AuthenticationException(
                'Invalid or expired EchoSign token'
            );
        }
    }
}