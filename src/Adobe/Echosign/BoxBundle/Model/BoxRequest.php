<?php
namespace Adobe\Echosign\BoxBundle\Model;

use Adobe\Echosign\BoxBundle\Validator\BoxRequestValidator;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * A skeleton for the data sent from Box
 */
class BoxRequest
{
    protected $userId;
    protected $userName;
    protected $fileId;
    protected $fileName;
    protected $fileExt;
    protected $fileUrl;
    protected $redirectUrl;
    protected $token;
    protected $authToken;

    /**
     * Construct the Box request skeleton using the POST information
     * or a plain array.
     * If used in the controller pass `$this->getRequest()->request`
     * as the parameter.
     *
     * @param mixed $post a `ParameterBag` instance or an array
     */
    public function __construct($post)
    {
        if (is_array($post))
        {
            $this->_constructWithArray($post);
        }
        elseif ($post instanceof ParameterBag)
        {
            $this->_constructWithParameterBag($post);
        }
        if (!BoxRequestValidator::validate($this)) {
            throw new BadRequestHttpException('Missed parameter or parameters in request');
        }
    }

    /**
     * Constructor helper class.
     *
     * @param \Symfony\Component\HttpFoundation\ParameterBag $post
     */
    private function _constructWithParameterBag(ParameterBag $post)
    {
        $this->userId      = filter_var($post->get("user_id"), FILTER_SANITIZE_NUMBER_INT);
        $this->userName    = filter_var($post->get("user_name"), FILTER_SANITIZE_STRING);
        $this->fileId      = filter_var($post->get("file_id"), FILTER_SANITIZE_NUMBER_INT);
        $this->fileName    = filter_var($post->get("file_name"), FILTER_SANITIZE_STRING);
        $this->fileExt     = filter_var($post->get("file_ext"), FILTER_SANITIZE_STRING);
        $this->fileUrl     = ($fileUrl = filter_var($post->get("file_url"), FILTER_SANITIZE_URL)) ? $fileUrl : '';
        $this->redirectUrl = ($redirectUrl = filter_var($post->get("redirect_url"), FILTER_SANITIZE_URL)) ? $redirectUrl : '';
        $this->authToken = filter_var($post->get("auth_code"), FILTER_SANITIZE_STRING);
    }

    /**
     * Constructor helper class.
     *
     * @param array $arr
     */
    private function _constructWithArray(array $arr)
    {
        $this->userId      = filter_var($arr["user_id"], FILTER_SANITIZE_NUMBER_INT);
        $this->userName    = filter_var($arr["user_name"], FILTER_SANITIZE_STRING);
        $this->fileId      = filter_var($arr["file_id"], FILTER_SANITIZE_NUMBER_INT);
        $this->fileName    = filter_var($arr["file_name"], FILTER_SANITIZE_STRING);
        $this->fileExt     = filter_var($arr["file_ext"], FILTER_SANITIZE_STRING);
        $this->fileUrl     = ($fileUrl = filter_var($arr["file_url"], FILTER_SANITIZE_URL)) ? $fileUrl : '';
        $this->redirectUrl = ($redirectUrl = filter_var($arr["redirect_url"], FILTER_SANITIZE_URL)) ? $redirectUrl : '';
        $this->token = $arr["file_auth_token"];
        $this->authToken = filter_var($arr["auth_code"], FILTER_SANITIZE_STRING);
    }

    /**
     * Returns the user id.
     *
     * @return string user id
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Sets the user id.
     *
     * @param $userId text with the user id
     * @return $this for chaining
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * Returns the user name.
     *
     * @return string user name
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Sets the user name.
     *
     * @param $userName text with the user name
     * @return $this for chaining
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
        return $this;
    }

    /**
     * Returns the file id.
     *
     * @return string file id
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * Sets the file id.
     *
     * @param $fileId text with the file id
     * @return $this for chaining
     */
    public function setFileId($fileId)
    {
        $this->fileId = $fileId;
        return $this;
    }

    /**
     * Returns the file name.
     *
     * @return string file name
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Sets the file name.
     *
     * @param $fileName text with the file name
     * @return $this for chaining
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
        return $this;
    }


    /**
     * Returns the file extension.
     *
     * @return string file extension
     */
    public function getFileExt()
    {
        return $this->fileExt;
    }

    /**
     * Sets the file extension.
     *
     * @param $fileExt text with the file extension
     * @return $this for chaining
     */
    public function setFileExt($fileExt)
    {
        $this->fileExt = $fileExt;
        return $this;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * Returns the file URL.
     *
     * @return string file URL
     */
    public function getFileUrl()
    {
        return $this->fileUrl;
    }

    /**
     * Sets the file URL.
     *
     * @param $fileUrl text with the file URL
     * @return $this for chaining
     */
    public function setFileUrl($fileUrl)
    {
        $this->fileUrl = $fileUrl;
        return $this;
    }


    /**
     * Returns the box redirect URL.
     *
     * @return string redirect URL
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * Sets the box redirect URL.
     *
     * @param $redirectUrl text with the redirect URL
     * @return $this for chaining
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $authToken
     */
    public function setAuthToken($authToken)
    {
        $this->authToken = $authToken;
    }

    /**
     * @return mixed
     */
    public function getAuthToken()
    {
        return $this->authToken;
    }
}
