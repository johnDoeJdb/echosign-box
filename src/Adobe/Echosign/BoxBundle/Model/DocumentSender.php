<?php
namespace Adobe\Echosign\BoxBundle\Model;

use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * A skeleton for the esign data
 */
class DocumentSender
{
     protected $emails;
     protected $roles;
     protected $ccs;
     protected $name;
     protected $message;
     protected $locale;
     protected $signatureFlow;
     protected $verifyWithPassword;
     protected $password;
     protected $passwordConfirm;
     protected $protectOpen;
     protected $interactive;
     protected $boxFolder;

     /**
      * Returns the array of emails.
      *
      * @return array emails
      */
     public function getEmails()
     {
         return $this->emails;
     }

     /**
      * Sets the whole emails array at once.
      *
      * @param array $emails
      * @return \Adobe\Echosign\BoxBundle\Model\DocumentSender $this for chaining
      */
     public function setEmails($emails)
     {
         $this->emails = $emails;
         return $this;
     }

     /**
      * Adds the emails one by one.
      *
      * @param string $email
      * @return \Adobe\Echosign\BoxBundle\Model\DocumentSender $this for chaining
      *
     public function addEmail($email)
     {
         if (!is_array($this->emails))
         {
             $this->emails = array();
         }
         array_push($this->emails, $email);

         return $this;
     }
      *
      */

     /**
      * Returns the array of roles.
      *
      * @return array roles
      */
     public function getRoles()
     {
         return $this->roles;
     }

     /**
      * Sets the whole roles array at once.
      *
      * @param array $roles
      * @return \Adobe\Echosign\BoxBundle\Model\DocumentSender $this for chaining
      */
     public function setRoles($roles)
     {
         $this->roles = $roles;
         return $this;
     }

     /**
      * Adds the roles one by one.
      *
      * @param string $role
      * @return \Adobe\Echosign\BoxBundle\Model\DocumentSender $this for chaining
      *
     public function addRole($role)
     {
         if (!is_array($this->roles))
         {
             $this->roles = array();
         }
         array_push($this->roles, $role);

         return $this;
     }
      *
      */


     /**
      * Gets the CCs
      *
      * @return array CCs
      */
     public function getCcs()
     {
         return $this->ccs;
     }

     /**
      * Sets the whole CCs array at once.
      *
      * @param array $ccs
      * @return \Adobe\Echosign\BoxBundle\Model\DocumentSender $this for chaining
      */
     public function setCcs($ccs)
     {
         $this->ccs = $ccs;
         return $this;
     }

     /**
      * Adds the CCs one by one.
      *
      * @param string $cc
      *
     public function addCc($cc)
     {
         if (!is_array($this->ccs))
         {
             $this->ccs = array();
         }
         array_push($this->ccs, $cc);
     }
      *
      */

     /**
      * Gets the document name.
      *
      * @return string document name
      */
     public function getName()
     {
         return $this->name;
     }

     /**
      * Sets the document name.
      *
      * @param string $name
      * @return \Adobe\Echosign\BoxBundle\Model\DocumentSender $this for chaining
      */
     public function setName($name)
     {
         $this->name = $name;
         return $this;
     }

     /**
      * Gets the message sent to the recipients.
      *
      * @return string message
      */
     public function getMessage()
     {
         return $this->message;
     }

     /**
      * Sets the message sent to the recipients.
      *
      * @param type $message
      * @return \Adobe\Echosign\BoxBundle\Model\DocumentSender $this for chaining
      */
     public function setMessage($message)
     {
         $this->message = $message;
         return $this;
     }

     /**
      * Gets the locale of the document.
      *
      * @return string locale
      */
     public function getLocale()
     {
         return $this->locale;
     }

     /**
      * Sets the locale of the document.
      *
      * @param string $locale
      * @return \Adobe\Echosign\BoxBundle\Model\DocumentSender $this for chaining
      */
     public function setLocale($locale)
     {
         $this->locale = $locale;
         return $this;
     }

     /**
      * Gets the signature flow.
      *
      * @return string the signature flow
      */
     public function getSignatureFlow()
     {
         return $this->signatureFlow;
     }

     /**
      * Sets the signature flow:
      * * SENDER_SIGNS_FIRST (default)
      * * SENDER_SIGNS_LAST
      * * SENDER_SIGNATURE_NOT_REQUIRED
      *
      * @param type $signatureFlow
      * @return \Adobe\Echosign\BoxBundle\Model\DocumentSender $this for chaining
      */
     public function setSignatureFlow($signatureFlow)
     {
         switch ($signatureFlow)
         {
             case "SENDER_SIGNS_FIRST":
             case "SENDER_SIGNS_LAST":
             case "SENDER_SIGNATURE_NOT_REQUIRED":
                 break;
             default:
                 $signatureFlow = "SENDER_SIGNS_FIRST";
                 break;
         }
         $this->signatureFlow = $signatureFlow;
         return $this;
     }

     /**
      * Gets the password verification status of the document.
      *
      * @return int password verification status
      */
     public function getVerifyWithPassword()
     {
         return $this->verifyWithPassword;
     }

     /**
      * Sets the password verification status.
      *
      * @param string $verify
      * @return \Adobe\Echosign\BoxBundle\Model\DocumentSender $this for chaining
      */
     public function setVerifyWithPassword($verify)
     {
         $this->verifyWithPassword = $verify;
         return $this;
     }

     /**
      * Gets the password
      *
      * @return string password
      */
     public function getPassword()
     {
         return $this->password;
     }

     /**
      * Sets the password
      *
      * @param type $password
      * @return \Adobe\Echosign\BoxBundle\Model\DocumentSender $this for chaining
      */
     public function setPassword($password)
     {
         $this->password = $password;
         return $this;
     }


     /**
      * Gets the verified password
      *
      * @return string password
      */
     public function getPasswordConfirm()
     {
         return $this->passwordConfirm;
     }

     /**
      * Sets the verified password
      *
      * @param type $password
      * @return \Adobe\Echosign\BoxBundle\Model\DocumentSender $this for chaining
      */
     public function setPasswordConfirm($password)
     {
         $this->passwordConfirm = $password;
         return $this;
     }

     /**
      * Password validator. It is not valid only if empty and required
      * by other fields configuration.
      */
     public function isPasswordSet(ExecutionContextInterface $context)
     {
         if ($this->getVerifyWithPassword() || $this->getProtectOpen())
         {
             if (strlen($this->getPassword()) < 3)
             {
                 $context->addViolationAt("password", "The password must contain at least 3 characters. Please select another password of at least 3 characters.", array(), null);
                 if (strlen($this->getPasswordConfirm()) < 3)
                 {
                     //$context->addViolationAt("passwordConfirm", "The password must contain at least 3 characters.", array(), null);
                 }
             }
             else
             {
                 if ($this->getPassword() != $this->getPasswordConfirm())
                 {
                     $context->addViolationAt("passwordConfirm", "The passwords do not match", array(), null);
                 }
             }
         }
     }

     /**
      * Gets the open protection status
      *
      * @return int open protection status
      */
     public function getProtectOpen()
     {
         return $this->protectOpen;
     }

     /**
      * Sets the open protection status
      *
      * @param type $protectOpen
      * @return \Adobe\Echosign\BoxBundle\Model\DocumentSender $this for chaining
      */
     public function setProtectOpen($protectOpen)
     {
         $this->protectOpen = $protectOpen;
         return $this;
     }

     /**
      * Gets the interactive value
      *
      * @return bool interactive value
      */
     public function getInteractive()
     {
         return $this->interactive;
     }

     /**
      * Sets the interactive value.
      *
      * @param bool $interactive
      * @return \Adobe\Echosign\BoxBundle\Model\DocumentSender $this for chaining
      */
     public function setInteractive($interactive)
     {
         $this->interactive = $interactive;
         return $this;
     }

    /**
     * @param mixed $boxFolder
     */
    public function setBoxFolder($boxFolder)
    {
        $this->boxFolder = $boxFolder;
    }

    /**
     * @return mixed
     */
    public function getBoxFolder()
    {
        return $this->boxFolder;
    }
}
