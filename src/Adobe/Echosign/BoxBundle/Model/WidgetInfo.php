<?php
namespace Adobe\Echosign\BoxBundle\Model;

class WidgetInfo
{
    protected $emails;
    protected $roles;
    protected $protection;
    protected $password;
    protected $protectOpen;
    protected $name;
    protected $ccs;
    protected $fileName;
    protected $file;
    protected $locale;
    protected $signatureFlow;
    protected $securityOptions;
    protected $callbackUrl;

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param mixed $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return mixed
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * @param mixed $emails
     */
    public function setEmails($emails)
    {
        $this->emails = $emails;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param mixed $roles
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

    /**
     * @return mixed
     */
    public function getProtection()
    {
        return $this->protection;
    }

    /**
     * @param mixed $protection
     */
    public function setProtection($protection)
    {
        $this->protection = $protection;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getProtectOpen()
    {
        return $this->protectOpen;
    }

    /**
     * @param mixed $protectOpen
     */
    public function setProtectOpen($protectOpen)
    {
        $this->protectOpen = $protectOpen;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCcs()
    {
        return $this->ccs;
    }

    /**
     * @param mixed $ccs
     */
    public function setCcs($ccs)
    {
        $this->ccs = $ccs;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return mixed
     */
    public function getSignatureFlow()
    {
        return $this->signatureFlow;
    }

    /**
     * @param mixed $signatureFlow
     */
    public function setSignatureFlow($signatureFlow)
    {
        $this->signatureFlow = $signatureFlow;
    }

    /**
     * @return mixed
     */
    public function getSecurityOptions()
    {
        return $this->securityOptions;
    }

    /**
     * @param mixed $securityOptions
     */
    public function setSecurityOptions($securityOptions)
    {
        $this->securityOptions = $securityOptions;
    }

    /**
     * @return mixed
     */
    public function getCallbackUrl()
    {
        return $this->callbackUrl;
    }

    /**
     * @param mixed $callbackUrl
     */
    public function setCallbackUrl($callbackUrl)
    {
        $this->callbackUrl = $callbackUrl;
    }


}
