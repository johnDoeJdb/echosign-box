<?php

namespace Adobe\Echosign\BoxBundle\Model;

/**
 * A skeleton for the user data, used in the registration form.
 */
class Authenticator
{
    protected $email;
    protected $password;
    protected $remember;
    protected $redirect;

    /**
     * Returns the user e-mail.
     *
     * @return string user e-mail
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the user e-mail.
     *
     * @param $email text with the user e-mail
     * @return $this for chaining
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Returns the user password.
     *
     * @return string user password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets the user password.
     *
     * @param $password text with the user password
     * @return $this for chaining
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Returns the remember me value.
     *
     * @return boolean remember me value
     */
    public function getRemember()
    {
        return (bool) $this->remember;
    }

    /**
     * Sets the remember me value.
     *
     * @param $remember boolean with the remember me value
     * @return $this for chaining
     */
    public function setRemember($remember)
    {
        $this->remember = (bool) $remember;
        return $this;
    }

    /**
     * Returns the redirect path.
     *
     * @return boolean remember me value
     */
    public function getRedirect()
    {
        return $this->redirect;
    }

    /**
     * Sets the redirect path.
     *
     * @param $path string with the optional redirection
     * @return $this for chaining
     */
    public function setRedirect($path)
    {
        $this->redirect = $path;
        return $this;
    }
}