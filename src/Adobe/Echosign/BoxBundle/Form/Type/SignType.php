<?php

namespace Adobe\Echosign\BoxBundle\Form\Type;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Handles send document form
 */
class SignType extends AbstractType
{
    protected $container;

    /**
     * SignType constructor.
     * @param $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @see \Symfony\Component\Form\AbstractType::getName()
     */
    public function getName()
    {
        return "sign";
    }

    /**
     * @see \Symfony\Component\Form\AbstractType::setDefaultOptions()
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            "data_class" => "Adobe\Echosign\BoxBundle\Model\DocumentSender",
            'csrf_protection' => true,
            'csrf_field_name' => 'esign'
        ));
    }

    /**
     * @see \Symfony\Component\Form\AbstractType::buildForm()
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->setMethod("POST")
            ->add("emails", "collection", array(
                "allow_add" => TRUE, "allow_delete" => TRUE,
                "error_bubbling" => FALSE,
                "type" => "text", "label" => "*To:",
            ))
            ->add("roles", "collection", array(
                "allow_add" => TRUE, "allow_delete" => TRUE,
                "type" => "choice", "label" => "Roles:",
                "options" => array(
                    "choices" => array(
                        "SIGNER" => "Signer",
                        "APPROVER" => "Approver"
                    )
                )
            ))
            ->add("ccs", "collection", array(
                "allow_add" => TRUE, "allow_delete" => TRUE,
                "error_bubbling" => FALSE,
                "type" => "text", "label" => "Cc:"
            ))
            ->add("name", "text", array(
                    "label" => "*Document Name:",
                    "data" => $this::getFileName()
                )
            )
            ->add("signatureFlow", "choice", array(
                "label" => FALSE,
                "choices" => array(
                    "SENDER_SIGNATURE_NOT_REQUIRED" => "Only the recipients will sign the document",
                    "SENDER_SIGNS_LAST" => "I will sign the document after the recipients",
                    "SENDER_SIGNS_FIRST" => "I will sign the document before the recipients"
                )
            ))
            ->add("locale", "choice", array(
                "label" => "*Language:",
                "data" => "en_US",
                "choices" => array(
                    "zh_CN" => "Chinese Simplified (简体中文)",
                    "zh_TW" => "Chinese Traditional (繁體中文)",
                    "da_DK" => "Danish (Dansk)",
                    "nl_NL" => "Dutch (Nederlands)",
                    "en_GB" => "English: UK (English: UK)",
                    "en_US" => "English: US",
                    "fi_FI" => "Finnish (Suomi)",
                    "fr_FR" => "French (Français)",
                    "de_DE" => "German (Deutsch)",
                    "is_IS" => "Icelandic (Íslenska)",
                    "in_ID" => "Indonesian (Bahasa Indonesia)",
                    "it_IT" => "Italian (Italiano)",
                    "ja_JP" => "Japanese (日本語)",
                    "ko_KR" => "Korean (한국어)",
                    "ms_MY" => "Malay: Malaysia (Melayu: Bahasa Malaysia)",
                    "nb_NO" => "Norwegian: Bokmal (Norsk: bokmål)",
                    "nn_NO" => "Norwegian: Nynorsk (Norsk: nynorsk)",
                    "no_NO" => "Norwegian (Norsk)",
                    "pl_PL" => "Polish (Polski)",
                    "pt_BR" => "Portuguese: Brazil (Português: Brasil)",
                    "pt_PT" => "Portuguese: Portugal (Português: Portugal)",
                    "ru_RU" => "Russian (Русский)",
                    "es_ES" => "Spanish (Español)",
                    "sv_SE" => "Swedish (Svenska)",
                    "th_TH" => "Thai (ไทย)",
                    "vi_VN" => "Vietnamese (Tiếng Việt)"
                )
            ))
            ->add("boxFolder", "box_folder", array(
                "required" => true
            ))
            ->add("verifyWithPassword", "checkbox", array("label" => FALSE, "required" => FALSE))
            ->add("password", "password", array("label" => "Signing password", "required" => FALSE, "attr" => array("autocomplete" => "off")))
            ->add("passwordConfirm", "password", array("label" => "Confirm password", "required" => FALSE, "attr" => array("autocomplete" => "off")))
            ->add("protectOpen", "checkbox", array("label" => FALSE, "required" => FALSE));
    }

    private function getFileName()
    {
        $session = $this->container->get('session');
        $box = unserialize($session->get('box'));
        $fullFileName = explode('.', $box->getFileName());
        $fullFileName[count($fullFileName)-1] = '';
        $fullFileName = join('.', $fullFileName);
        $fileName = substr($fullFileName, 0, strlen($fullFileName) - 1);

        return $fileName;
    }
}
