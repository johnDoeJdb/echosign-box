<?php

namespace Adobe\Echosign\BoxBundle\Form\Type\General;

use Adobe\Echosign\BoxBundle\Command\UploaderDocumentCommand;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\DependencyInjection\Container;

class BoxFolderType extends AbstractType
{
    /**
     * @var Container
     */
    private $container;

    function __construct($container)
    {
        $this->container = $container;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $userManager = $this->container->get('adobe_echosign_box.user.manager');
        $boxUser = $userManager->fetchCurrentBoxUser();
        $boxUserToken = $userManager->getTokenForBoxUser($boxUser);
        UploaderDocumentCommand::createAttachmentFolderAndGetId($boxUserToken, $this->container);

        $resolver->setDefaults(array(
            "error_bubbling" => false,
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
    }

    public function getParent()
    {
        return 'hidden';
    }

    public function getName()
    {
        return 'box_folder';
    }
}