<?php

namespace Adobe\Echosign\BoxBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Adobe\Echosign\BoxBundle\DependencyInjection\Session\EncryptedSessionProxy;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeSessionHandler;

class AdobeEchosignBoxBundle extends Bundle
{
    public function boot()
    {
//        $session = $this->container->get("session");
//
//        // Wrap the session handler with the encryption
//        $proxy = new EncryptedSessionProxy(new NativeSessionHandler());
//        session_set_save_handler($proxy);
//        $session->start();

        parent::boot();
    }
}
