<?php

namespace Adobe\Echosign\BoxBundle\Manager;

use Adobe\Echosign\BoxBundle\Api\EchoSign;
use Adobe\Echosign\BoxBundle\Entity\BoxUser;
use Adobe\Echosign\BoxBundle\Entity\Document;
use Adobe\Echosign\BoxBundle\Entity\EchoSignUser;
use Symfony\Component\DependencyInjection\Container;

/**
 * A skeleton for the user data, used in the registration form.
 */
class SynchronizerDocuments
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function uploadDocuments(BoxUser $user, array $documents, $type = null)
    {
        $ids = array();
        foreach ($documents as $document) {
            $ids[] = $document->getId();
        }

        $this->backgroundDownloader($user->getUserId(), $ids, $type);
    }

    private function backgroundDownloader($userId, $documentsIds, $type = null)
    {
        $parameters = array(
            'userId' => $userId,
            'documentsIds' => $documentsIds,
            'type' => $type,
        );
        $parameters = base64_encode(json_encode($parameters));
        exec('php '.WEB_DIRECTORY.'/../app/console upload:documents '.$parameters.' &> /dev/null &');
    }
}