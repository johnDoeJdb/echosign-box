<?php

namespace Adobe\Echosign\BoxBundle\Manager;

use Adobe\Echosign\BoxBundle\Entity\BoxUser;
use Adobe\Echosign\BoxBundle\Entity\EchoSignUser;
use Adobe\Echosign\BoxBundle\Model\BoxRequest;
use Symfony\Component\DependencyInjection\Container;

/**
 * A skeleton for the user data, used in the registration form.
 */
class UserManager
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return BoxUser
     */
    public function fetchCurrentBoxUser()
    {
        $box = unserialize($this->container->get('session')->get('box'));
        $boxUser = null;
        if ($box && $box instanceof BoxRequest) {
            $boxUser = $this->findBoxUser($box->getUserId());
        }

        return $boxUser;
    }

    /**
     * @return EchoSignUser
     */
    public function fetchCurrentEchoSignUser()
    {
        $box = unserialize($this->container->get('session')->get('box'));
        $echoSignUser = null;
        if ($box && $box instanceof BoxRequest) {
            $echoSignUser = $this->getEchoSignUser($box->getUserId());
        }

        return $echoSignUser;
    }

    /**
     * @return BoxUser
     */
    public function findBoxUser($userId)
    {
        $entityManager = $this->container->get('doctrine.orm.entity_manager');
        $user = $entityManager->getRepository('AdobeEchosignBoxBundle:BoxUser')->findOneBy(array(
          'userId' => $userId,
        ));

        return $user;
    }

    /**
     * @return EchoSignUser
     */
    public function getEchoSignUser($boxUserId)
    {
        $entityManager = $this->container->get('doctrine.orm.entity_manager');
        $user = $entityManager->getRepository('AdobeEchosignBoxBundle:EchoSignUser')->fetchEchoSignUser($boxUserId);

        return $user;
    }

    /**
     * @return EchoSignUser
     */
    public function getEchoSignUserByEmail($email)
    {
        $entityManager = $this->container->get('doctrine.orm.entity_manager');
        $user = $entityManager->getRepository('AdobeEchosignBoxBundle:EchoSignUser')->findOneBy(array(
            'email' => $email
        ));

        return $user;
    }

    /**
     * @return BoxUser
     */
    private function refreshAccessToken(BoxUser $user)
    {
        $now = new \DateTime();
        $cryptManager = $this->container->get('adobe_echosign_box.crypt.manager');
        $boxApi = $this->container->get('adobe_echosign_box.box_api');
        $refreshToken = $cryptManager->decrypt($user->getRefreshToken());
        $lastUsageRefreshToken = $user->getRefreshTokenInitialized();
        $lastUsageRefreshToken->modify('+60 days');
        if ($now->getTimestamp() > $lastUsageRefreshToken->getTimestamp()) {
            $token = null;
        } else {
            $response = json_decode($boxApi->getRefreshToken($refreshToken));
            if (!$response || property_exists($response, 'error')) {
                return null;
            }
            $token = $response->access_token;
            $user->setRefreshToken($cryptManager->encrypt($response->refresh_token));
            $user->setToken($cryptManager->encrypt($token));
            $user->setRefreshTokenInitialized($now);
            $user->setTokenInitialized($now);
        }

        return $user;
    }

    public function getAccessToken(BoxUser $user)
    {
        $cryptManager = $this->container->get('adobe_echosign_box.crypt.manager');
        $entityManager = $this->container->get('doctrine.orm.entity_manager');
        $now = new \DateTime();
        $tokenInitialized = $user->getTokenInitialized();
        $tokenInitialized->modify('+1 hour');
        $token = null;
        if ($now->getTimestamp() < $tokenInitialized->getTimestamp()) {
            $token = $cryptManager->decrypt($user->getToken());
        } else {
            $user = $this->refreshAccessToken($user);
            if (method_exists($user, 'getToken')) {
                $token = $user->getToken() ? $cryptManager->decrypt($user->getToken()) : null;
            } else {
                return null;
            }
        }
        if ($user) {
            $entityManager->persist($user);
            $entityManager->flush();
        }

        return $token;
    }

    public function getTokenForBoxUser(BoxUser $boxUser)
    {
        $box = unserialize($this->container->get('session')->get('box'));
        $boxApiWrapper = $this->container->get('adobe_echosign_box.box_api');
        $token = null;
        if (
            !$box
            || !$boxUser
            || !$boxUser instanceof BoxUser
            || !$boxUser->getToken()
        ) {
            $token = null;
        } elseif (!$token = $this->getAccessToken($boxUser)) {
            $token = null;
        } if (!$token || !$boxApiWrapper->checkValidToken($box->getFileId(), $token)) {
            $token = null;
        }



        return $token;
    }

    public function getTokenForEchoSignUser($echoSignUser)
    {
        $cryptManager = $this->container->get('adobe_echosign_box.crypt.manager');
        if (!$echoSignUser || !$echoSignUser instanceof EchoSignUser || !$echoSignUser->getToken() || !$echoSignUser->isValidToken()) {
            $token = null;
        } else {
            $token = $cryptManager->decrypt($echoSignUser->getToken());
        }

        return $token;
    }

    public function getRefreshTokenForEchoSignUser($echoSignUser)
    {
        $cryptManager = $this->container->get('adobe_echosign_box.crypt.manager');

        if (!$echoSignUser || !$echoSignUser instanceof EchoSignUser || !$echoSignUser->getRefreshToken()) {
            $token = null;
        } else {
            $token = $cryptManager->unserializeAndDecrypt($echoSignUser->getRefreshToken());
        }

        return $token;
    }

    public function updateToken($token)
    {
        $cryptManager = $this->container->get('adobe_echosign_box.crypt.manager');
        $entityManager = $this->container->get('doctrine.orm.entity_manager');
        $token = $cryptManager->encrypt($token);
        $user = $this->fetchCurrentBoxUser();
        $user->setToken($token);

        $entityManager->persist($user);
        $entityManager->flush();
    }

    public function updateEchoSignToken($token, $expiration = null)
    {
        $cryptManager = $this->container->get('adobe_echosign_box.crypt.manager');
        $entityManager = $this->container->get('doctrine.orm.entity_manager');
        $token = $cryptManager->encrypt($token);
        $user = $this->fetchCurrentEchoSignUser();
        $user->setToken($token);
        if (!$expiration) {
            $expiration = ((new \DateTime())->modify('+1 hour'));
        }
        $user->setExpireToken($expiration);

        $entityManager->persist($user);
        $entityManager->flush();
    }
}