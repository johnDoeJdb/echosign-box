<?php
namespace Adobe\Echosign\BoxBundle\Manager;

use Symfony\Component\DependencyInjection\Container;

class DocumentPurgeManager
{
    /**
     * @var Container
     */
    private $container;
    private $entityManager;

    public function __construct($container, $entityManager)
    {
        $this->container = $container;
        $this->entityManager = $entityManager;
    }

    public function purge($limit)
    {
        $users = $this->entityManager->getRepository('AdobeEchosignBoxBundle:BoxUser')->fetchUsersForScan($limit);
        $count = 0;
        foreach ($users as $user) {
            $count = $this->purgeUserDocuments($user);
        }

        return $count;
    }

    public function purgeUserDocuments($boxUser)
    {
        $boxApi = $this->container->get('adobe_echosign_box.box_api');
        $userManager = $this->container->get('adobe_echosign_box.user.manager');
        $token = $userManager->getAccessToken($boxUser);
        $documents = $this->entityManager->getRepository('AdobeEchosignBoxBundle:Document')->fetchDocumentsByUserId($boxUser->getUserId());
        $count = 0;
        foreach ($documents as $document) {
            $boxFileId = $document->getBoxFileId();
            $info = json_decode($boxApi->getFileInfo($boxFileId.'1', $token));
            if (property_exists($info, 'type')) {
                if ($info->type == 'error') {
                    $trashedFile = json_decode($boxApi->getTrashedFile($boxFileId.'1', $token));
                    if (property_exists($trashedFile, 'type')) {
                        $this->entityManager->getRepository('AdobeEchosignBoxBundle:Document')->removeDocumentById($document->getId());
                        $count++;
                    }
                }
            }
        }
        $boxUser->setLastScan(new \DateTime());
        $this->entityManager->persist($boxUser);
        $this->entityManager->flush();

        return $count;
    }
}
