<?php
namespace Adobe\Echosign\BoxBundle\Manager;

use Symfony\Component\DependencyInjection\Container;

class CryptManager
{
    private $key;

    function __construct(Container $container)
    {
        $this->key = $container->getParameter('crypt_key');
    }

    function encrypt($encrypt) {
        $mc_key = $this->key;
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND);
        $passcrypt = trim(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $mc_key, trim($encrypt), MCRYPT_MODE_ECB, $iv));
        $encode = base64_encode($passcrypt);

        return $encode;
    }

    function decrypt($decrypt) {
        $mc_key = $this->key;
        $decoded = base64_decode($decrypt);
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND);
        $decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $mc_key, trim($decoded), MCRYPT_MODE_ECB, $iv));

        return $decrypted;
    }

    function serializeAndCrypt($object) {
        return base64_encode($this->encrypt((serialize(($object)))));
    }

    function unserializeAndDecrypt($string) {
        return unserialize($this->decrypt((base64_decode(($string)))));
    }
}
