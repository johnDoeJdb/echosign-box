<?php
namespace Adobe\Echosign\BoxBundle\Manager;

use Adobe\Echosign\BoxBundle\Entity\BoxUser;
use Symfony\Component\DependencyInjection\Container;

class RestoreManager
{
    /**
     * @var Container
     */
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function restoreFoldersIfTrashed(BoxUser $user, $token)
    {
        $entityManager = $this->container->get('doctrine.orm.entity_manager');
        $boxApiWrapper = $this->container->get('adobe_echosign_box.box_api');

        $folderIds = $entityManager->getRepository('AdobeEchosignBoxBundle:Folder')->getFoldersByUserId($user->getUserId());
        $trashedFolders = $boxApiWrapper->filterTrashed($folderIds, $token);
        $boxApiWrapper->restoreFolders($trashedFolders, $token);
    }
}
