<?php
namespace Adobe\Echosign\BoxBundle\Manager;

use Symfony\Component\HttpFoundation\Session\Session;

class SessionManager
{
    /**
     * @var Session
     */
    private $session;

    public function __construct($session)
    {
        $this->session = $session;
    }

    public function wipe()
    {
        $box = $this->session->get('box');
        $this->session->clear();
        $this->session->set('box', $box);
    }
}
