<?php

namespace Adobe\Echosign\BoxBundle\Manager;

use Adobe\Echosign\BoxBundle\Model\DocumentSender;
use Symfony\Component\DependencyInjection\Container;
use Adobe\Echosign\BoxBundle\Model\WidgetInfo;

/**
 * A skeleton for the user data, used in the registration form.
 */
class WidgetManager
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getWidgetInfo(DocumentSender $docSender)
    {
        $userManager = $this->container->get('adobe_echosign_box.user.manager');
        $boxApiWrapper = $this->container->get('adobe_echosign_box.box_api');
        $boxUser = $userManager->fetchCurrentBoxUser();
        $boxUserToken = $userManager->getTokenForBoxUser($boxUser);
        $box = unserialize($this->container->get('session')->get('box'));
        $widgetInfo = new WidgetInfo();
        $urlParameters = array(
            'userId' => $boxUser->getUserId(),
            'folderId' => $docSender->getBoxFolder(),
        );
        $serverIp = $_SERVER['SERVER_ADDR'];

        $widgetInfo->setCallbackUrl('https://'.$serverIp.'/box/callback/signed_echo_sign?'.http_build_query($urlParameters));
        $widgetInfo->setEmails($docSender->getEmails());
        $widgetInfo->setRoles($docSender->getRoles());
        $widgetInfo->setCcs(array_values($docSender->getCcs()));
        $widgetInfo->setName($docSender->getName());
        $widgetInfo->setFileName($box->getFileName());
        $widgetInfo->setFile($boxApiWrapper->getFile($box->getFileId(), $boxUserToken));
        $widgetInfo->setFileName($box->getFileName());
        $widgetInfo->setLocale($docSender->getLocale());
        $widgetInfo->setSignatureFlow($docSender->getSignatureFlow());
        $widgetInfo->setProtection($docSender->getVerifyWithPassword());
        $widgetInfo->setPassword($docSender->getPassword());
        $widgetInfo->setProtectOpen($docSender->getProtectOpen());

        return $widgetInfo;
    }
}