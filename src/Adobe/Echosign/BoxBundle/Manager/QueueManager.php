<?php
namespace Adobe\Echosign\BoxBundle\Manager;

use Adobe\Echosign\BoxBundle\Entity\BoxUser;
use Symfony\Component\DependencyInjection\Container;

class QueueManager
{
    /**
     * @var Container
     */
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function downloadQueuedDocuments(BoxUser $user)
    {
        $entityManager = $this->container->get('doctrine.orm.entity_manager');
        $uploader = $this->container->get('adobe_echosign_box.sync.doc');

        $documents = $entityManager->getRepository('AdobeEchosignBoxBundle:Document')->fetchQueuedDocuments($user->getUserId());
        if (count($documents)) {
            $uploader->uploadDocuments($user, $documents);
        }
    }
}
