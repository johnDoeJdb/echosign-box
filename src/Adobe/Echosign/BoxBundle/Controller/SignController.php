<?php
namespace Adobe\Echosign\BoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Adobe\Echosign\BoxBundle\Model\DocumentSender;

class SignController extends Controller
{
    use ControllerTrait;

    public function signAction(Request $request)
    {
        $box = $this->getBoxRequest();
        $userManager = $this->getUserManager();
        $echoSignUser = $userManager->fetchCurrentEchoSignUser();
        $echoSignUserToken = $userManager->getTokenForEchoSignUser($echoSignUser);

        $docSender = new DocumentSender();
        $form = $this->createForm("sign", $docSender, array(
            "action" => $this->generateUrl("document_sign")
        ));

        if ("POST" == $request->getMethod())
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                $widgetInfo = $this->getWidgetManager();
                $widgetInfo = $widgetInfo->getWidgetInfo($docSender);
                $echoSignApi = $this->getEchoSignApiWrapper();
                $result = $echoSignApi->createEmbeddedWidget($echoSignUserToken, $widgetInfo)->embeddedWidgetCreationResult;
                if ($errorMessage = $result->errorMessage)
                {
                    $this->addFlash("error", $errorMessage);
                } else if($result->success) {
                    $this->addFlash("js", $result->javascript);

                    return $this->setP3PHeader()->redirectToRoute("document_widget");
                }
                if (property_exists($result, 'errorMessage'))
                {
                    $errorMessage = $result->errorMessage;
                    $this->addFlash("error", $errorMessage);
                }
            }
        }

        return $this->setP3PHeader()->render("AdobeEchosignBoxBundle:Sign:index.html.twig", array(
            "form" => $form->createView(),
            "loggedUser"   => $echoSignUser->getEmail(),
            "fileName"     => $box->getFileName(),
            "fileExt"      => $box->getFileExt()
        ));
    }

    /**
     * Handle the interactive signing form.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function interactiveAction(Request $request)
    {
        $echoSignUser = $this->getUserManager()->fetchCurrentEchoSignUser();
        $loggedUser = $echoSignUser->getEmail();

        $js = $this->getFlash("js");

        $response = $this->setP3PHeader()->setQuirksHeader()->render("AdobeEchosignBoxBundle:Sign:widget.html.twig", array(
            "loggedUser"   => $loggedUser,
            "js"           => $js
        ));

        return $response;
    }
}