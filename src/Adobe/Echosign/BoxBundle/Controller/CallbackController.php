<?php
namespace Adobe\Echosign\BoxBundle\Controller;

use Adobe\Echosign\BoxBundle\Entity\BoxUser;
use Adobe\Echosign\BoxBundle\Entity\Document;
use Adobe\Echosign\BoxBundle\Entity\EchoSignUser;
use Adobe\Echosign\BoxBundle\Entity\Folder;
use Adobe\Echosign\BoxBundle\Validator\BoxRequestValidator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Adobe\Echosign\BoxBundle\Model\BoxRequest;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class CallbackController extends Controller
{
    use ControllerTrait;

    public function indexAction()
    {
        $request = $this->get('request');
        $url = $this->generateUrl("document_sign", $request->query->all(), UrlGeneratorInterface::ABSOLUTE_URL);
        return $this->render("@AdobeEchosignBox/Callback/index.html.twig", array(
            'redirect_url' => $url,
        ));
    }

    public function signedEchoSignAction()
    {
        $request = $this->get('request');
        $entityManager = $this->getDoctrine()->getManager();
        $syncManager = $this->getSyncronizerDocuments();
        $userManager = $this->getUserManager();
        $userId = filter_var($request->get('userId'), FILTER_SANITIZE_NUMBER_INT);
        $folderId = filter_var($request->get('folderId'), FILTER_SANITIZE_NUMBER_INT);
        $documentKey = $request->get('documentKey');
        $boxUser = $userManager->findBoxUser($userId);
        $echoSignUser = $userManager->getEchoSignUser($userId);
        if (!$document = $entityManager->getRepository('AdobeEchosignBoxBundle:Document')->findOneBy(array('documentKey' => $documentKey))) {
            $document = new Document();
            $document->setUser($echoSignUser);
            $document->setDocumentKey($documentKey);
            if (!$folder = $entityManager->getRepository('AdobeEchosignBoxBundle:Folder')->find($folderId)) {
                $folder = new Folder();
                $folder->setBoxId($folderId);
            }
            try {
                $document->setFolder($folder);
            } catch (\Exception $e) {
                $logger = $this->getLog();
                $logger->error($e->getMessage());
            }
            $entityManager->persist($folder);
            $entityManager->persist($echoSignUser);
        }
        // Set document in queue
        $document->setQueued(true);
        $entityManager->persist($document);
        $entityManager->flush();
        if ($echoSignUser->isValidToken()) {
            $syncManager->uploadDocuments($boxUser, array($document));
        }

        return Response::create('Got callback from echoSign API');
    }

    public function boxCallbackTokenAction(Request $request)
    {
        $entityManager = $this->get('doctrine.orm.entity_manager');
        $cryptManager = $this->getCryptManager();
        $now = new \DateTime();
        $code = filter_var($request->query->get('code'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
        $state = $request->query->get('state');
        $box = $this->cryptManager->unserializeAndDecrypt($state);
        if (!$box instanceof BoxRequest || !BoxRequestValidator::validate($box)) {
            throw new BadRequestHttpException("Did not get correct state from request");
        }
        $boxApi = $this->getBoxApiWrapper();
        $response = json_decode($boxApi->getAccessToken($code));
        $token = $response->access_token;
        $refreshToken = $response->refresh_token;

        if (!$boxUser = $entityManager->getRepository('AdobeEchosignBoxBundle:BoxUser')->findOneBy(array('userId' => $box->getUserId()))) {
            $boxUser = new BoxUser();
        }

        $boxUser->setUserId($box->getUserId());
        $boxUser->setToken($cryptManager->encrypt($token));
        $boxUser->setRefreshToken($cryptManager->encrypt($refreshToken));
        $boxUser->setTokenInitialized($now);
        $boxUser->setRefreshTokenInitialized($now);

        $entityManager->persist($boxUser);
        $entityManager->flush();

        return $this->redirectToRoute('document_sign');
    }

    public function echoSignCallbackTokenAction(Request $request)
    {
        $entityManager = $this->get('doctrine.orm.entity_manager');
        $cryptManager = $this->getCryptManager();
        $code = filter_var($request->query->get('code'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
        $state = $request->query->get('state');
        if ($request->query->get('error') && $request->query->get('error_description')) {
            $errorMessage = filter_var($request->query->get('error_description'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
            throw new BadRequestHttpException($errorMessage);
        }
        $box = $this->getCryptManager()->unserializeAndDecrypt($state);
        if (!$box instanceof BoxRequest || !BoxRequestValidator::validate($box)) {
            throw new BadRequestHttpException("Gotten invalid state from request");
        }
        $echoSignApi = $this->getEchoSignApiWrapper();
        $response = $echoSignApi->getAccessOAuthToken($code);
        if ($response && is_object($response)) {
            if (property_exists($response, 'access_token') && property_exists($response, 'refresh_token')) {
                $token = $response->access_token;
                $refreshToken = $response->refresh_token;
                $expirationIn = sprintf('+%d seconds', $response->expires_in);
                $expiration = (new \DateTime())->modify($expirationIn);
                if (!$boxUser = $entityManager->getRepository('AdobeEchosignBoxBundle:BoxUser')->findOneBy(array('userId' => $box->getUserId()))) {
                    throw new BadRequestHttpException("BoxUser not found");
                }
                if (!$echoSignUser = $entityManager->getRepository('AdobeEchosignBoxBundle:EchoSignUser')->fetchEchoSignUser($box->getUserId())) {
                    $echoSignUser = new EchoSignUser();
                }
                $userInfo = $echoSignApi->getUserInfo($token);

                $echoSignUser->setToken($cryptManager->encrypt($token));
                $echoSignUser->setRefreshToken($cryptManager->serializeAndCrypt($refreshToken));
                $echoSignUser->setExpireToken($expiration);
                $echoSignUser->setBoxUser($boxUser);
                $echoSignUser->setEmail($userInfo->getUserInfoResult->data->email);

                $entityManager->persist($boxUser);
                $entityManager->persist($echoSignUser);
                $entityManager->flush();

                return $this->redirectToRoute('document_sign');
            }
            if (property_exists($response, 'error_description')) {
                $errorMessage = filter_var($response->error_description, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
                throw new BadRequestHttpException($errorMessage);
            } else {
                throw new BadRequestHttpException("Token was not issued");
            }
        }
    }
}