<?php

namespace Adobe\Echosign\BoxBundle\Controller;

use Adobe\Echosign\BoxBundle\Api\Box;
use Adobe\Echosign\BoxBundle\Api\EchoSign;
use Adobe\Echosign\BoxBundle\Authenticator\Protector;
use Adobe\Echosign\BoxBundle\Manager\CryptManager;
use Adobe\Echosign\BoxBundle\Manager\QueueManager;
use Adobe\Echosign\BoxBundle\Manager\UserManager;
use Adobe\Echosign\BoxBundle\Manager\WidgetManager;
use Adobe\Echosign\BoxBundle\Model\BoxRequest;
use Adobe\Echosign\BoxBundle\Model\WidgetInfo;
use Adobe\Echosign\BoxBundle\Validator\BoxRequestValidator;
    ;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Session\Session;

trait ControllerTrait
{
    protected $session;
    protected $log;
    protected $protector;
    protected $syncronizer;
    protected $boxConnector;
    protected $boxRequest;
    protected $connector;
    protected $userManager;
    protected $queueManager;
    protected $cryptManager;
    protected $widgetManager;

    protected function setQuirksHeader()
    {
        header("X-UA-Compatible: IE=7", TRUE);
        return $this;
    }

    protected function setP3PHeader()
    {
        header("X-UA-Compatible: IE=edge");
        header('P3P: CP="CAO PSA OUR IND ONL"');
        return $this;
    }

    protected function setFameAllow()
    {
        header( 'X-Frame-Options: ALLOW-ALL' );
        return $this;
    }

    /**
     * @return Logger
     */
    protected function getLog()
    {
        if (!isset($this->log))
        {
            $this->log = $this->get("logger");
        }
        return $this->log;
    }

    /**
     * @return Protector
     */
    protected function getProtector()
    {
        if (!isset($this->protector))
        {
            $this->protector = $this->get("adobe_echosign_box.protector");
        }
        return $this->protector;
    }

    /**
     * @return EchoSign
     */
    protected function getEchoSignApiWrapper()
    {
        if (!isset($this->connector))
        {
            $this->connector = $this->get("adobe_echosign_box.echosign_api");
        }
        return $this->connector;
    }

    /**
     * @return Box
     */
    protected function getBoxApiWrapper()
    {
        if (!isset($this->boxConnector))
        {
            $this->boxConnector = $this->get("adobe_echosign_box.box_api");
        }
        return $this->boxConnector;
    }

    protected function getSyncronizerDocuments()
    {
        if (!isset($this->syncronizer))
        {
            $this->syncronizer = $this->get("adobe_echosign_box.sync.doc");
        }
        return $this->syncronizer;
    }

    /**
     * @return Session
     */
    protected function getSession()
    {
        if (!isset($this->session))
        {
            $this->session = $this->get("session");
        }
        return $this->session;
    }

    protected function getSessionManager()
    {
        if (!isset($this->session))
        {
            $this->session = $this->get("adobe_echosign_box.session.manager");
        }
        return $this->session;
    }

    /**
     * @return UserManager
     */
    protected function getUserManager()
    {
        if (!isset($this->userManager))
        {
            $this->userManager = $this->get("adobe_echosign_box.user.manager");
        }
        return $this->userManager;
    }

    /**
     * @return QueueManager
     */
    protected function getQueueManager()
    {
        if (!isset($this->queueManager))
        {
            $this->queueManager = $this->get("adobe_echosign_box.queue.manager");
        }
        return $this->queueManager;
    }

    /**
     * @return BoxRequest
     */
    protected function getBoxRequest()
    {
        if (!isset($this->boxRequest))
        {
            $box = unserialize($this->getSession()->get('box'));
            if ($box instanceof BoxRequest && BoxRequestValidator::validate($box)) {
                $this->boxRequest = $box;
            }
        }
        return $this->boxRequest;
    }

    /**
     * @return CryptManager
     */
    protected function getCryptManager()
    {
        if (!isset($this->cryptManager))
        {
            $this->cryptManager = $this->get("adobe_echosign_box.crypt.manager");
        }
        return $this->cryptManager;
    }

    protected function getFlash($name, $default = NULL)
    {
        $flash = $this->getSession()->getFlashBag()->get($name);
        if (is_array($flash))
        {
            $return = array_shift($flash);
            if (empty($return))
            {
                $return = $default;
            }
        }
        else
        {
            $return = $default;
        }
        return $return;
    }

    /**
     * @return WidgetManager
     */
    protected function getWidgetManager()
    {
        if (!isset($this->widgetManager))
        {
            $this->widgetManager = $this->get('adobe_echosign_box.widget.manager');
        }
        return $this->widgetManager;
    }
}
