<?php

namespace Adobe\Echosign\BoxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AjaxController extends Controller
{
    use ControllerTrait;

    public function getNodeChildrenAction(Request $request)
    {
        $folderId = $request->query->get('folderId');
        $boxUser = $this->getUserManager()->fetchCurrentBoxUser();
        $token = $this->getCryptManager()->decrypt($boxUser->getToken());
        $node = $this->getBoxApiWrapper()->getNodeChildren($folderId, $token);

        return new JsonResponse($node);
    }
}