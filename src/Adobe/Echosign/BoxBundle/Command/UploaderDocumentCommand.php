<?php
namespace Adobe\Echosign\BoxBundle\Command;

use Adobe\Echosign\BoxBundle\Api\Box;
use Adobe\Echosign\BoxBundle\Entity\Document;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;

class UploaderDocumentCommand extends ContainerAwareCommand
{
    CONST PATH_TO_DOWNLOAD = '/tmp/echobox/';
    CONST DEFAULT_FOLDER_NAME = 'Adobe Document Cloud Agreements';

    protected function configure()
    {
        $this
            ->setName('upload:documents')
            ->setDescription('Upload documents by ids')
            ->addArgument('documents', InputArgument::REQUIRED, 'documents ids array in json format');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $userManager = $this->getContainer()->get('adobe_echosign_box.user.manager');
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        $echoSignApi = $this->getContainer()->get('adobe_echosign_box.echosign_api');
        $cryptManager = $this->getContainer()->get('adobe_echosign_box.crypt.manager');
        $argumentBase64 = $input->getArgument('documents');
        $data = json_decode(base64_decode($argumentBase64));
        $userId = $data->userId;
        $documentsId = $data->documentsIds;
        $type = $data->type;
        $user = $userManager->findBoxUser($userId);
        $tempFolder = $this->createTempFolder($userId);
        $documents = $entityManager->getRepository('AdobeEchosignBoxBundle:Document')->fetchDocumentsByIds($documentsId);

        try {
            $user = $userManager->findBoxUser($userId);
            $tempFolder = $this->createTempFolder($userId);
        } catch(\Exception $e) {
            file_put_contents('/tmp/box.error', $e->getMessage());
        }

        foreach ($documents as $document) {
            $documentKey = $document->getDocumentKey();
            $echoSignUser = $this->getEchoSignUserByDocument($document->getId());
            if (!$echoSignUser->isValidToken()) {
                continue;
            }
            try {
                $info = $echoSignApi->getDocumentInfo($documentKey, $cryptManager->decrypt($echoSignUser->getToken()));
            } catch (\Exception $e) {
                $logger = $this->getLog();
                $logger->error($e->getMessage());
            }
            $status = $info->documentInfo->status;
            $waitingSince = $this->getWaitingSince($info);
            if ($document->getWaitingSince() == $waitingSince || !$status == 'SIGNED') {
                continue;
            }
            if ($type && ($type !== $status)) {
                continue;
            }
            if (!$documentName = $document->getName()) {
                $documentName = $this->getFileName($info->documentInfo->name, $echoSignUser->getId());
            }
            if ($status == 'SIGNED') {
                $typeDocuments = ['auditTrail', 'document'];
            } else {
                $typeDocuments = ['auditTrail'];
            }
            foreach ($typeDocuments as $type) {
                if ($type == 'document') {
                    $name = $documentName.'_signed';
                } else {
                    $name = $documentName.'_audit';
                }
                $path = $this->generatePath($tempFolder, $name);
                $this->download($type, $path, $status, $documentKey, $cryptManager->decrypt($echoSignUser->getToken()));
                if (!$fileId = $this->upload($path, $document->getFolder()->getBoxId(), $cryptManager->decrypt($user->getToken()))) {
                    continue;
                }
                $document->setBoxFileId($fileId);
                $document->setLastStatus($status);
                $document->setWaitingSince($status);
                $document->setQueued(false);
                $document->setName($documentName);
                $entityManager->persist($document);
                $entityManager->flush();
            }
        }
        $this->clearTempFolder($user->getUserId());
    }

    public static function createTempFolder($id)
    {
        $path = UploaderDocumentCommand::PATH_TO_DOWNLOAD . $id;
        @mkdir($path, 0777, true);

        return $path;
    }

    public static function clearTempFolder($id)
    {
        exec("rm -R " . UploaderDocumentCommand::PATH_TO_DOWNLOAD . $id);
    }

    public static function createAttachmentFolderAndGetId($token, Container $container)
    {
        $boxApi = $container->get('adobe_echosign_box.box_api');
        $folder = json_decode($boxApi->createFolder(self::DEFAULT_FOLDER_NAME, $token));
        $id = ($folder->type === 'error') ? $folder->context_info->conflicts[0]->id : $folder->id;

        return $id;
    }

    public static function downloadFile($url, $path, $data = null)
    {
        $document = ($data) ? $data : @file_get_contents($url);

        @file_put_contents($path, $document);
    }

    public function getFileName($name, $userId)
    {
        if ($repeats = $this
            ->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository('AdobeEchosignBoxBundle:Document')
            ->findNameByPattern($name, $userId)
        ) {
            $numberRepeats = intval($repeats) + 1;
            $name .= '-' . $numberRepeats;
        }
        $name = str_replace('/', ' ', $name);
        $name = str_replace('\\', ' ', $name);

        return $name;
    }

    private function generatePath($folder, $name)
    {
        return $folder . '/' . $name . '.pdf';
    }

    private function getEchoSignUserByDocument($documentKey)
    {
        $echoSignUser = $this
            ->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository('AdobeEchosignBoxBundle:EchoSignUser')
            ->getEchoSignUserByDocument($documentKey);
        return $echoSignUser;
    }

    private function download($type = 'auditTrail', $path, $status, $documentKey, $token)
    {
        $echoSignApi = $this->getContainer()->get('adobe_echosign_box.echosign_api');
        if ($type == 'document') {
            $response = $echoSignApi->getDocumentUrls($documentKey, $token);
            $urls = $response->getDocumentUrlsResult->urls;
            if (!$urls) {
                return;
            }
            $file = $urls->DocumentUrl->url;
            $this->downloadFile($file, $path);
        } else {
            $auditResult = $echoSignApi->getAuditTrail($documentKey, $token);
            if ($file = $auditResult->getAuditTrailResult->auditTrailPdf) {
                $this->downloadFile(null, $path, $file);
            }
        }
    }

    private function upload($path, $folderId, $token)
    {
        $boxApi = $this->getContainer()->get('adobe_echosign_box.box_api');
        $response = $boxApi->uploadFile($path, $folderId, $token);
        $response = json_decode($response);
        if (!$response) {
            return null;
        } elseif (isset($response->entries[0]->id)) {
            $fileId = $response->entries[0]->id;
        } elseif (isset($response->context_info->conflicts->id)) {
            $fileId = $response->context_info->conflicts->id;
            $boxApi->uploadNewVersionFile($path, $folderId, $fileId, $token);
        }

        return $fileId;
    }

    private function getWaitingSince($info)
    {
        $nextParticipantInfo = ($nextParticipantInfo = $info->documentInfo->nextParticipantInfos) ? $nextParticipantInfo->NextParticipantInfo  : null;
        if (is_object($nextParticipantInfo)) {
            $waitingSince = $nextParticipantInfo->waitingSince;
        } else {
            $waitingSince = 'v1';
        }

        return $waitingSince;
    }
}