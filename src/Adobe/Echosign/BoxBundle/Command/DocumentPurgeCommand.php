<?php

namespace Adobe\Echosign\BoxBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;

class DocumentPurgeCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('box:documents:purge')
            ->addOption('limit', null, InputOption::VALUE_OPTIONAL, 'Limit user which will be scan', 100)
            ->setDescription('Purge unused documents');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $limit = $input->getOption('limit');
        $purgeManager = $this->getContainer()->get('adobe_echosign_box.documents_purge.manager');
        $count = $purgeManager->purge($limit);

        $output->write('<info>Purged documents:'.$count.'</info>'."\r\n");
    }
}