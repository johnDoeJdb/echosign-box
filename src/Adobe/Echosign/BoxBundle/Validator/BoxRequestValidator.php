<?php
namespace Adobe\Echosign\BoxBundle\Validator;

use Adobe\Echosign\BoxBundle\Model\BoxRequest;

class BoxRequestValidator
{
    public static function validate(BoxRequest $box)
    {
        if (!$box->getUserId() || !$box->getUserName() || !$box->getFileExt() || !$box->getFileId() || !$box->getFileName())
        {
            return false;
        } else {
            return true;
        }
    }
}
