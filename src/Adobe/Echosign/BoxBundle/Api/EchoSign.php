<?php

namespace Adobe\Echosign\BoxBundle\Api;

use Adobe\Echosign\BoxBundle\Model\WidgetInfo;
use Symfony\Component\DependencyInjection\Container;

/**
 * EchoSign connection wrapper; makes it easier to manage the SOAP API actions.
 */
class EchoSign
{
    protected $api;
    protected $soap;
    protected $wsdl;
    protected $authHost;
    protected $clientId;
    protected $clientSecret;
    protected $redirectUrl;
    protected $lastStatus;
    protected $lastError;

    function __construct(Container $container)
    {
        $this->container = $container;
    }

    protected function _setupApi()
    {
        if ("load" != $this->api) { // lazy load
            // Specify you parameters
            $this->api = "load";
            $this->clientId = $this->container->getParameter('echo_sign.auth.client_id');
            $this->clientSecret = $this->container->getParameter('echo_sign.auth.client_secret');
            $this->redirectUrl = $this->container->getParameter('echo_sign.auth.redirect_url');
            $this->authHost = $this->container->getParameter('echo_sign.auth.host');
            $this->wsdl = $this->container->getParameter('echo_sign.doc.wsdl');
            $this->soap = new \SoapClient($this->wsdl);
        }
        $this->lastError = "";
        $this->lastStatus = "";
        return $this;
    }

    /**
     * Gets the last exception message that was caught during the execution
     * of the methods. Usually when a SOAP action fails the methods only return
     * boolean false. However the information about the error are saved,
     * so they can be retrieved with this method and used for eg. logging.
     *
     * @return last exception message
     */
    public function getLastError()
    {
        return $this->lastError;
    }

    /**
     * @return last status
     */
    public function getLastStatus()
    {
        return $this->lastStatus;
    }


    public function getAuthorizationOAuthUrl($state, $referUrl = null)
    {
        $this->_setupApi();
        $data = array(
            'response_type' => 'code',
            'redirect_uri' => $this->redirectUrl,
            'client_id' => $this->clientId,
            'scope' => 'agreement_read user_read widget_write',
            'state' => $state,
        );
        $url = $this->authHost.'/public/oauth?'.http_build_query($data);

        return $url;
    }

    public function getAccessOAuthToken($code)
    {
        $this->_setupApi();
        $data = array(
            'grant_type' => "authorization_code",
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'redirect_uri' => $this->redirectUrl,
            'code' => $code,
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->authHost.'/oauth/token');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = json_decode(curl_exec($ch));
        curl_close ($ch);

        return $response;
    }

    public function refreshOAuthToken($refreshToken)
    {
        $this->_setupApi();
        $data = array(
            'grant_type' => "refresh_token",
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'refresh_token' => $refreshToken,
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->authHost.'/oauth/refresh');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        $result = json_decode($response);
        curl_close ($ch);

        return $result;
    }

    public function getAuditTrail($documentKey, $token)
    {
        $this->_setupApi();

        $response = $this->soap->getAuditTrail(array(
            "apiKey" => $token,
            "documentKey" => $documentKey,
        ));

        return $response;
    }


    public function getUserInfo($token)
    {
        $this->_setupApi();

        $response = $this->soap->getUserInfo(array(
            "apiKey" => $token,
            "options" => array(
                'email' => null,
            ),
        ));

        return $response;
    }

    public function getUsersInAccount($token)
    {
        $this->_setupApi();

        $response = $this->soap->getUsersInAccount(array(
            "apiKey" => $token,
        ));

        return $response;
    }

    /**
     * Wrapper for `getDocumentUrls` API action.
     * @param $documentKey
     * @return mixed
     */
    public function getDocumentUrls($documentKey, $token)
    {
        $this->_setupApi();
        $response = $this->soap->getDocumentUrls(array(
            "apiKey" => $token,
            "documentKey" => $documentKey,
            "options" => array(
                'versionKey', 'participantEmail', 'combine', 'attachSupportingDocuments', 'auditReport'
            ),
        ));

        return $response;
    }

    public function getDocumentInfo($documentKey, $token)
    {
        $this->_setupApi();

        $response = $this->soap->getDocumentInfo(array(
            "apiKey" => $token,
            "documentKey" => $documentKey,
        ));


        return $response;
    }

    public function createEmbeddedWidget($token, WidgetInfo $widgetInfo)
    {
        $this->_setupApi();
        $recipients = $this->_formRecipients($widgetInfo->getEmails(), $widgetInfo->getRoles());
        $securityOptions = $this->_formSecurityOptions($widgetInfo->getProtection(), $widgetInfo->getPassword(), $widgetInfo->getProtectOpen());

        $response = $this->soap->createEmbeddedWidget(array(
            "apiKey" => $token,
            "widgetInfo" => array(
                "name" => $widgetInfo->getName(),
                "ccs" => $widgetInfo->getCcs(),
                "counterSigners" => $recipients,
                "fileInfos"  => array(array(
                    "fileName" => $widgetInfo->getFileName(),
                    "file"      => $widgetInfo->getFile()
                )),
                "signatureType" => "ESIGN",
                "signatureFlow" => $widgetInfo->getSignatureFlow(),
                "locale" => $widgetInfo->getLocale(),
                "securityOptions" => $securityOptions,
                "callbackInfo"  => array(
                    "signedDocumentUrl" => $widgetInfo->getCallbackUrl(),
                ),
            ),
        ));

        return $response;
    }

    /**
     * Send document helper function.
     * The important thing is that the roles need to match the emails,
     * so if the array of emails looks like:
     * [5] => "some@email.com",
     * [8] => "some@email.com"
     * the array of needs to have the corresponding role attached to the
     * keys:
     * [5] => "SIGNER",
     * [8] => "APPROVER"
     *
     * @param array $emails the array of emails
     * @param array $roles the corresponding array of roles
     * @return array product that can be used directly in sendDocument
     */
    protected function _formRecipients(array $emails, array $roles)
    {
        $recipients = array();

        foreach ($emails as $i => $email)
        {
            array_push($recipients, array(
                "email" => $email,
                "role"  => $roles[$i]
            ));
        }

        return $recipients;
    }

    /**
     * Send document helper function. It forms the security options from the
     * three parameters.
     *
     * @param int $protection
     * @param string $password
     * @param bool $protectOpen
     * @return array security options to use in document info
     */
    protected function _formSecurityOptions($protection, $password, $protectOpen)
    {
        $securityOptions = array();

        if (boolval($protectOpen))
        {
            $securityOptions["protectOpen"] = boolval($protectOpen);
            $securityOptions["password"]    = $password;
        }

        if (boolval($protection))
        {
            $securityOptions["passwordProtection"] = "ALL_USERS";
            $securityOptions["password"] = $password;
        }

        return $securityOptions;
    }
}