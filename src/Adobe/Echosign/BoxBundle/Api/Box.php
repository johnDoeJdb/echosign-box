<?php
namespace Adobe\Echosign\BoxBundle\Api;
use Symfony\Component\DependencyInjection\Container;

/**
 * Wrapper for connect to Box API
 */
class Box
{
    function __construct(Container $container)
    {
        $this->container = $container;
        $this->client_id = $container->getParameter('box.client_id');
        $this->secret = $container->getParameter('box.secret');
        $this->redirect_uri = $container->getParameter('box.redirect_uri');
        $this->api_host = $container->getParameter('box.api_host');
        $this->auth_host = $container->getParameter('box.auth_host');
        $this->file_upload_host = $container->getParameter('box.file_upload_host');
    }

    public function getAuthUrl($box, $referUrl = null)
    {
        $parameters = array(
           'response_type' => 'code',
           'client_id' => $this->client_id,
           'redirect_uri' => $this->redirect_uri,
           'state' => $box,
        );
        $response = $this->auth_host.'/authorize'.'?'.http_build_query($parameters);

        return $response;
    }

    public function getAccessToken($code)
    {
        $parameters = array(
           'grant_type' => 'authorization_code',
           'code' => $code,
           'client_id' => $this->client_id,
           'client_secret' => $this->secret,
        );
        $response = $this->sendRequest($this->auth_host.'/token', $parameters, 'POST', null, $type = 'text');

        return $response;
    }

    public function getRefreshToken($refreshToken)
    {
        $parameters = array(
            'refresh_token' => $refreshToken,
            'grant_type' => 'refresh_token',
            'client_id' => $this->client_id,
            'client_secret' => $this->secret,
        );
        $response = $this->sendRequest($this->auth_host.'/token', $parameters, 'POST', null, $type = 'text');

        return $response;
    }

    public function checkValidToken($fileId, $token)
    {
        $response = $this->sendRequest($this->api_host."/files/{$fileId}", null, 'GET', $token);
        $valid = false;
        if ($response) {
            if (!stristr($response, '"type":"error"') && !stristr($response, '400 Bad Request')) {
                $valid = true;
            }
        }

        return $valid;
    }

    public function createFolder($name, $token)
    {
        $parameters = array(
            'name' => $name,
            'parent' => array('id' => '0'),
        );
        $response = $this->sendRequest($this->api_host.'/folders', $parameters, 'POST', $token);

        return $response;
    }

    public function getFileInfo($fileId, $token)
    {
        $response = $this->sendRequest($this->api_host."/files/{$fileId}", null, 'GET', $token);

        return $response;
    }

    public function uploadFile($path, $folderId, $token)
    {
        $parameters = array(
            'filename' => "@" . $path,
            'parent_id' => $folderId,
        );

        $response = $this->sendRequest($this->file_upload_host.'/content', $parameters, 'POST', $token, $type = 'text');

        return $response;
    }

    public function getFile($fileId, $token)
    {
        $response = $this->sendRequest($this->api_host.'/files/'.$fileId.'/content', null, 'GET', $token);

        return $response;
    }

    public function getNodeChildren($folderId, $token)
    {
        $fetchFolders = function ($folderId) use ($token, &$fetchFolders) {
            $folders = array();
            $response = $this->sendRequest($this->api_host."/folders/{$folderId}/items", null, 'GET', $token);
            $object = json_decode($response);
            $entries = property_exists($object, 'entries') ? $object->entries : array();
            foreach ($entries as $entry) {
                if ($entry->type == 'folder') {
                    $folder = array();
                    $folder['id'] = $entry->id;
                    $folder['text'] = $entry->name;
                    $folder['children'] = true;
                    $folders[] = $folder;
                }
            }

            return $folders;
        };
        $folders = $fetchFolders($folderId);

        return $folders;
    }

    public function filterTrashed($folderIds, $token)
    {
        $folders = array();
        $allTrashed = json_decode($this->sendRequest($this->api_host."/folders/trash/items", null, 'GET', $token));
        if ($allTrashed) {
            foreach ($allTrashed->entries as $item) {
                foreach ($folderIds as $folderId) {
                    if ($item->id == $folderId) {
                        $folders[] = $item->id;
                    }
                }
            }
        }

        return $folders;
    }

    public function restoreFolders($foldersIds, $token)
    {
        foreach ($foldersIds as $folderId) {
            $this->sendRequest($this->api_host."/folders/{$folderId}", null, 'POST', $token);
        }
    }

    public function getTrashedFolders($foldersIds, $token)
    {
        foreach ($foldersIds as $folderId) {
            $folders = $this->sendRequest($this->api_host."/folders/{$folderId}/trash", null, 'GET', $token);
            var_dump($folders);
        }
    }

    public function getTrashedFile($file, $token)
    {
        return $this->sendRequest($this->api_host."/files/{$file}/trash", null, 'GET', $token);
    }

    public function uploadNewVersionFile($path, $folderId, $fileId, $token)
    {
        $parameters = array(
            'filename' => "@" . $path,
            'parent_id' => $folderId,
        );

        $response = $this->sendRequest($this->file_upload_host.'/'.$fileId.'/content', $parameters, 'POST', $token, $type = 'text');

        return $response;
    }

    private function sendRequest($url, $parameters = null, $method = 'GET', $token = null, $type = 'JSON')
    {
        $ch = curl_init();
        if ($method == 'POST' || $method == 'PUT') {
            if ($parameters && $type == 'JSON') {
                $parameters = json_encode($parameters);
            }
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        }
        if ($method == 'GET' && $parameters) {
            $url .= '?' . http_build_query($parameters);
        }
        if ($token) {
            $headers[] = 'Authorization: Bearer '.$token;
            curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
        }
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }
}
